"""test cases for helloworld"""

from helloworld import app

def test_home():
    """test the root endpoint"""
    response = app.test_client().get('/', content_type='html/text')
    assert response.status_code == 200
    assert b'DevSecOps Hello World Flask App' in response.data

def test_other():
    """test the handling of non-existent endpoints"""
    response = app.test_client().get('a', content_type='html/text')
    assert response.status_code == 404
    assert b'The page named a does not exist' in response.data
